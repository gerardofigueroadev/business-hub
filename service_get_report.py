from sqlalchemy import text


#query for reports
def query_all_category(db):
    data = []
    security = db.engine.execute(get_security())
    data.append(map_result_complete('Seguridad', security))

    availability_free_parking_places = db.engine.execute(get_availability_free_parking_places())
    data.append(map_result_complete('Parqueos Gratis Disponibles', availability_free_parking_places))

    availability_paid_parking_places = db.engine.execute(get_availability_paid_parking_places())
    data.append(map_result_complete('Parqueos de Pago Disponibles', availability_paid_parking_places))

    public_transport = db.engine.execute(get_public_transport())
    data.append(map_result_complete('Transporte Publico', public_transport))

    entertainment_places = db.engine.execute(get_entertainment_places())
    data.append(map_result_complete('Cuidado de Salud', entertainment_places))

    green_areas = db.engine.execute(get_green_areas())
    data.append(map_result_complete('Areas Verdes', green_areas))

    universities = db.engine.execute(get_universities())
    data.append(map_result_complete('Centros Educativos', universities))

    offices = db.engine.execute(get_offices())
    data.append(map_result_complete('Oficinas', offices))

    food_court = db.engine.execute(get_food_court())
    data.append(map_result_complete('Restaurantes', food_court))

    influx = db.engine.execute(get_influx())
    data.append(map_result_complete('Afluencia de Personas', influx))

    return data


def map_result_complete(category_name, results):
    names = [category_name]
    return {
        'category': names,
        'result': map_from_data_list(results)
    }


def map_from_data_list(results):
    response = []
    for result in results:
        response.append(map_from_data(result))

    return response


def map_from_data(result):

    results = [float(result[0])]

    return {
        'data': results,
        'label': result[1]
    }


def get_security():
    return text('SELECT security AS data, n.name AS label '\
                'FROM weights w INNER JOIN "neighborhoodUnits" n ON n.id = w.neighborhood_id '\
                'ORDER BY security DESC '\
                'limit 5; ')


def get_availability_free_parking_places():
    return text('SELECT availability_free_parking_places AS data, n.name AS label '\
                'FROM weights w INNER JOIN "neighborhoodUnits" n ON n.id = w.neighborhood_id '\
                'ORDER BY security DESC '\
                'limit 5; ')


def get_availability_paid_parking_places():
    return text('SELECT availability_paid_parking_places AS data, n.name AS label ' \
                'FROM weights w INNER JOIN "neighborhoodUnits" n ON n.id = w.neighborhood_id ' \
                'ORDER BY security DESC ' \
                'limit 5; ')


def get_public_transport():
    return text('SELECT public_transport AS data, n.name AS label ' \
                'FROM weights w INNER JOIN "neighborhoodUnits" n ON n.id = w.neighborhood_id ' \
                'ORDER BY security DESC ' \
                'limit 5; ')


def get_entertainment_places():
    return text('SELECT entertainment_places AS data, n.name AS label ' \
                'FROM weights w INNER JOIN "neighborhoodUnits" n ON n.id = w.neighborhood_id ' \
                'ORDER BY security DESC ' \
                'limit 5; ')


def get_green_areas():
    return text('SELECT green_areas AS data, n.name AS label ' \
                'FROM weights w INNER JOIN "neighborhoodUnits" n ON n.id = w.neighborhood_id ' \
                'ORDER BY security DESC ' \
                'limit 5; ')


def get_universities():
    return text('SELECT universities AS data, n.name AS label ' \
                'FROM weights w INNER JOIN "neighborhoodUnits" n ON n.id = w.neighborhood_id ' \
                'ORDER BY security DESC ' \
                'limit 5; ')


def get_schools():
    return text('SELECT schools AS data, n.name AS label ' \
                'FROM weights w INNER JOIN "neighborhoodUnits" n ON n.id = w.neighborhood_id ' \
                'ORDER BY security DESC ' \
                'limit 5; ')


def get_offices():
    return text('SELECT offices AS data, n.name AS label ' \
                'FROM weights w INNER JOIN "neighborhoodUnits" n ON n.id = w.neighborhood_id ' \
                'ORDER BY security DESC ' \
                'limit 5; ')


def get_food_court():
    return text('SELECT food_court AS data, n.name AS label ' \
                'FROM weights w INNER JOIN "neighborhoodUnits" n ON n.id = w.neighborhood_id ' \
                'ORDER BY security DESC ' \
                'limit 5; ')


def get_influx():
    return text('SELECT influx AS data, n.name AS label ' \
                'FROM weights w INNER JOIN "neighborhoodUnits" n ON n.id = w.neighborhood_id ' \
                'ORDER BY security DESC ' \
                'limit 5; ')