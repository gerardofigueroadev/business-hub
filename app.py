from flask import Flask
from flask_restful import reqparse, Api, Resource
from flask_sqlalchemy import SQLAlchemy
from flask_cors import CORS

import service
import service_get_report


app = Flask(__name__)
CORS(app)
app.config['SQLALCHEMY_DATABASE_URI'] = 'postgresql://postgres:postgres@localhost/business2'
api = Api(app)
db = SQLAlchemy(app)


parser = reqparse.RequestParser()
parser.add_argument('availability_free_parking_places')
parser.add_argument('availability_paid_parking_places')
parser.add_argument('security')
parser.add_argument('public_transport')
parser.add_argument('entertainment_places')
parser.add_argument('green_areas')
parser.add_argument('universities')
parser.add_argument('schools')
parser.add_argument('offices')
parser.add_argument('food_court')
parser.add_argument('influx')


class ResultAnalysis(Resource):
    def get(self):
        return service_get_report.query_all_category(db)

    def post(self):
        args = parser.parse_args()
        return service.analysis_put(db, args)


class ResultApi(Resource):
    def get(self, id):
        return service.get_weights_by_id(db, id)


api.add_resource(ResultApi, '/result/<int:id>')
api.add_resource(ResultAnalysis, '/analysis')


if __name__ == '__main__':
    app.run(debug=True)