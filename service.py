from sqlalchemy import text
import random
import json


def best_average():
    return text(
        'select id, neighborhood_id, ' \
        'round( ' \
        'avg(availability_paid_parking_places + ' \
        'availability_free_parking_places + ' \
        'security + ' \
        'public_transport + ' \
        'entertainment_places + ' \
        'green_areas + ' \
        'universities + ' \
        'schools + ' \
        'offices + ' \
        'food_court + ' \
        'influx) ' \
        '/ 11, ' \
        '2) prom ' \
        'from weights ' \
        'group by id, neighborhood_id ' \
        'order by avg(availability_paid_parking_places + availability_free_parking_places + security + public_transport + entertainment_places + green_areas + universities + schools + offices + food_court + influx) / 11 ' \
        'desc ' \
        'limit 2; ')


def insert_request(neighborhood_id):
    return text('insert into request(neighborhood_id, location_type, location_category_id) ' \
                'values ({}, 1, 1)'.format(neighborhood_id))


def select_weights_by_id(neighborhood_id):
    return text('select * from weights where neighborhood_id = {};'.format(neighborhood_id))


def select_weights_all():
    return text('SELECT * FROM weights;')


def map_from_get_full_api(result):
    return {
        'id': result[0],
        'neighborhood_id': result[1],
        'availability_free_parking_places': float(result[2]),
        'availability_paid_parking_places': float(result[3]),
        'security': float(result[4]),
        'public_transport': float(result[5]),
        'entertainment_places': float(result[6]),
        'green_areas': float(result[7]),
        'universities': float(result[8]),
        'schools': float(result[9]),
        'offices': float(result[10]),
        'food_court': float(result[11]),
        'influx': float(result[12])
    }


def map_from_get_locations_api(result):
    return {
        'id': result[0],
        'latitude': float(result[1]),
        'longitude': float(result[2])
    }


def select_locations(neighborhood_id):
    return text('SELECT id, latitude, longitude FROM locations WHERE "neighborhoodUnitId" = {}'.format(neighborhood_id))


class WeightParams:
    def __init__(self, availability_free_parking_places, availability_paid_parking_places,
                 security, public_transport, entertainment_places, green_areas, universities, schools,
                 offices, food_court, influx):
        self.availability_free_parking_places = availability_free_parking_places
        self.availability_paid_parking_places = availability_paid_parking_places
        self.security = security
        self.public_transport = public_transport
        self.entertainment_places = entertainment_places
        self.green_areas = green_areas
        self.universities = universities
        self.schools = schools
        self.offices = offices
        self.food_court = food_court
        self.influx = influx


def map_from_json(weight):
    return {
        'id': weight.id,
        'neighborhood_id': weight.neighborhood_id,
        'availability_free_parking_places': round(weight.availability_free_parking_places, 2),
        'availability_paid_parking_places': round(weight.availability_paid_parking_places, 2),
        'security': round(weight.security, 2),
        'public_transport': round(weight.public_transport, 2),
        'entertainment_places': round(weight.entertainment_places, 2),
        'green_areas': round(weight.green_areas, 2),
        'universities': round(weight.universities, 2),
        'schools': round(weight.schools, 2),
        'offices': round(weight.offices, 2),
        'food_court': round(weight.food_court, 2),
        'influx': round(weight.influx, 2),
        'average': round(weight.average, 2),
        'latitude': weight.latitude,
        'longitude': weight.longitude,
        'color': weight.color
    }


class Weights:
    def __init__(self, id, neighborhood_id, availability_free_parking_places, availability_paid_parking_places,
                 security, public_transport, entertainment_places, green_areas, universities, schools,
                 offices, food_court, influx):
        self.id = id
        self.neighborhood_id = neighborhood_id
        self.availability_free_parking_places = availability_free_parking_places
        self.availability_paid_parking_places = availability_paid_parking_places
        self.security = security
        self.public_transport = public_transport
        self.entertainment_places = entertainment_places
        self.green_areas = green_areas
        self.universities = universities
        self.schools = schools
        self.offices = offices
        self.food_court = food_court
        self.influx = influx
        self.average = 0,
        self.latitude = 0,
        self.longitude = 0,
        self.color = ''


def get_arguments(args):
    return WeightParams(
        set_average(float(args['availability_free_parking_places'])),
        set_average(float(args['availability_paid_parking_places'])),
        set_average(float(args['security'])),
        set_average(float(args['public_transport'])),
        set_average(float(args['entertainment_places'])),
        set_average(float(args['green_areas'])),
        set_average(float(args['universities'])),
        set_average(float(args['schools'])),
        set_average(float(args['offices'])),
        set_average(float(args['food_court'])),
        set_average(float(args['influx']))
    )


def set_average(number):
    if number == 1:
        number = 0.2

    if number == 2:
        number = 0.4

    if number == 3:
        number = 0.6

    if number == 4:
        number = 0.8

    if number == 5:
        number = 1

    return number


def reduce_value(number_one, number_two):
    return float(number_one) * float(number_two)


def average_weights(weight):
    return (
                weight.availability_free_parking_places +
                weight.availability_paid_parking_places +
                weight.security +
                weight.public_transport +
                weight.entertainment_places +
                weight.green_areas +
                weight.universities +
                weight.schools +
                weight.offices +
                weight.food_court +
                weight.influx
            ) / 11


def analysis_put(db, args):
    arguments = get_arguments(args)
    results = db.engine.execute(select_weights_all())
    weights = []
    weight_json = []
    for res in results:
        weight = Weights(
            res[0],
            res[1],
            reduce_value(res[2], arguments.availability_free_parking_places),
            reduce_value(res[3], arguments.availability_paid_parking_places),
            reduce_value(res[4], arguments.security),
            reduce_value(res[5], arguments.public_transport),
            reduce_value(res[6], arguments.entertainment_places),
            reduce_value(res[7], arguments.green_areas),
            reduce_value(res[8], arguments.universities),
            reduce_value(res[9], arguments.schools),
            reduce_value(res[10], arguments.offices),
            reduce_value(res[11], arguments.food_court),
            reduce_value(res[12], arguments.influx),
        )

        weight.average = average_weights(weight)
        weight = dinamic_values(weight, db)
        weights.append(weight)

    weights.sort(key=lambda x: x.average, reverse=True)
    weights = weights[:2]
    weights[0].color = 'green'
    weights[1].color = 'blue'

    for w in weights:
        results = db.engine.execute(select_locations(w.neighborhood_id))

        locations = []
        random_location = None

        for location in results:
            locations.append(map_from_get_locations_api(location))

        if len(locations) > 0:
            random_location = random.choice(locations)

        w.longitude = random_location['longitude']
        w.latitude = random_location['latitude']

        weight_json.append(map_from_json(w))
    return weight_json


def get_weights_by_id(db, id):
    result = db.engine.execute(select_weights_by_id(id))
    data = []
    for w in result:
        data.append(map_from_get_full_api(w))

    return data[0]


def max_value_by_category(id):
    return text('SELECT count(*) FROM locations WHERE "locationCategoryId" = {} GROUP BY "neighborhoodUnitId" ORDER BY count(*) DESC limit 1;'.format(id))


def max_value_by_neighborhood_unit(neighborhood_id, category_id):
    return text('select count(*) from locations where "neighborhoodUnitId" = {} and "locationCategoryId" = {};'.format(neighborhood_id, category_id))


def dinamic_values(weight, db):

    max_value_university = db.engine.execute(max_value_by_category(2))
    current_value_university = db.engine.execute(max_value_by_neighborhood_unit(weight.neighborhood_id, 2))
    weight.schools = get_value(max_value_university, current_value_university)
    weight.universities = get_value(max_value_university, current_value_university)

    max_value_help_care = db.engine.execute(max_value_by_category(3))
    current_value_help_care = db.engine.execute(max_value_by_neighborhood_unit(weight.neighborhood_id, 3))
    weight.entertainment_places = get_value(max_value_help_care, current_value_help_care)

    max_value_green_areas = db.engine.execute(max_value_by_category(4))
    current_value_green_areas = db.engine.execute(max_value_by_neighborhood_unit(weight.neighborhood_id, 4))
    weight.green_areas = get_value(max_value_green_areas, current_value_green_areas)

    max_value_parking = db.engine.execute(max_value_by_category(6))
    current_value_parking = db.engine.execute(max_value_by_neighborhood_unit(weight.neighborhood_id, 6))
    weight.availability_paid_parking_places = get_value(max_value_parking, current_value_parking)

    return weight


def get_value(max_value, current_value):
    parking_max = 0
    parking_current = 0

    for max in max_value:
        parking_max = float(max[0])

    for value in current_value:
        parking_current = float(value[0])

    if parking_max > 0:
        return round((parking_current / parking_max) * 5)
    else:
        return 0
